﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OpenBanking.API.Models.Commands;
using OpenBanking.API.Models.ViewModel;

namespace OpenBanking.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DDAController : ControllerBase
    {


        [HttpGet, Route("{agencia}/{conta}")]
        public SacadorViewModel Index([FromRoute] string agencia, string conta)
        {
            return new SacadorViewModel();
        }

        [HttpGet, Route("{agencia}/{conta}/agregados")]
        public SacadorViewModel Agregados([FromRoute] string agencia, string conta)
        {
            return new SacadorViewModel();
        }

        [HttpPost, Route("{agencia}/{conta}/agregados")]
        public IActionResult Agregados(IncluirDDAAgregadoCommand command)
        {
            return Ok();
        }

        [HttpDelete, Route("{agencia}/{conta}/agregados/{cpfcnpj}")]
        public IActionResult Agregados([FromRoute] string agencia, string conta, string cpfcnpj)
        {
            return Ok();
        }

        [HttpGet, Route("{agencia}/{conta}/titulos")]
        public IActionResult Titulos([FromRoute] string agencia, string conta)
        {
            return Ok();
        }

        [HttpGet, Route("{agencia}/{conta}/titulos/{cpfcnpj}")]
        public IActionResult Titulos([FromRoute] string agencia, string conta, string cpfcnpj)
        {
            return Ok();
        }

    }
}