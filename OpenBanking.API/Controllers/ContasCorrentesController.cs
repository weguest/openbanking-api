﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OpenBanking.API.Models.Commands;
using OpenBanking.API.Models.ViewModel;

namespace OpenBanking.API.Controllers
{
    [ApiExplorerSettings(GroupName = "v2")]
    [Route("api/v2/[controller]")]
    [ApiController]
    public class ContasCorrentesController : ControllerBase
    {

        [HttpGet, Route("{agencia}/{conta}/transferencia/{id}")]
        public TransferenciaViewModel Transferencia([FromRoute] string id)
        {
            return new TransferenciaViewModel();
        }

        [HttpPost, Route("{agencia}/{conta}/transferencia")]
        public TransferenciaViewModel Transferencia(IncluirTransferenciaCommand command)
        {
            return new TransferenciaViewModel();
        }

        [HttpGet, Route("{agencia}/{conta}/saldo")]
        public SaldoViewModel Saldo()
        {
            return new SaldoViewModel();
        }

        [HttpGet, Route("{agencia}/{conta}/extrato")]
        public ExtratoViewModel Extrato([FromQuery] DateTime? Inicio, DateTime? fim )
        {
            return new ExtratoViewModel();
        }

    }
}