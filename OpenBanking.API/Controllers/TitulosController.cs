﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OpenBanking.API.Models.Commands;
using OpenBanking.API.Models.ViewModel;

namespace OpenBanking.API.Controllers
{
    /// <summary>
    /// Títulos
    /// </summary>
    [ApiExplorerSettings(GroupName = "v2")]
    [Route("api/v2/[controller]")]
    [ApiController]
    public class TitulosController : ControllerBase
    {
        #region contasimples
        /// <summary>
        /// Obter Conta Simples
        /// </summary>
        /// <example>Ex:{agencia}/{conta}/contasimples/123456 </example>
        [HttpGet, Route("{agencia}/{conta}/contasimples/{id}")]
        public ContaSimplesViewModel ContaSimples(string id)
        {
            return new ContaSimplesViewModel();
        }

        /// <summary>
        /// Incluir Conta Simples
        /// </summary>
        [HttpPost, Route("{agencia}/{conta}/contasimples")]
        public IActionResult ContaSimples(IncluirContaSimplesCommand command)
        {
            return Ok();
        }

        /// <summary>
        /// Cancelar Conta Simples
        /// </summary>
        [HttpPut, Route("{agencia}/{conta}/contasimples")]
        public IActionResult ContaSimples(AtualizarContaSimplesCommand command)
        {
            return Ok();
        }

        #endregion

        #region contaconsumo

        /// <summary>
        /// Obter Conta Consumo
        /// </summary>
        [HttpGet, Route("{agencia}/{conta}/contaconsumo/{id}")]
        public ContaConsumoViewModel ContaConsumo(string id)
        {
            return new ContaConsumoViewModel();
        }

        /// <summary>
        /// Incluir Conta Consumo
        /// </summary>
        [HttpPost, Route("{agencia}/{conta}/contaconsumo")]
        public IActionResult ContaConsumo(IncluirContaConsumoCommand command)
        {
            return Ok();
        }

        /// <summary>
        /// Atualizar Conta Consumo
        /// </summary>
        [HttpPut, Route("{agencia}/{conta}/contaconsumo")]
        public IActionResult ContaConsumo(AtualizarContaConsumoCommand command)
        {
            return Ok();
        }
        #endregion

        #region tributos
        /// <summary>
        /// Obter Tributo
        /// </summary>
        [HttpGet, Route("{agencia}/{conta}/tributo/{id}")]
        public TributoViewModel Tributo(string id)
        {
            return new TributoViewModel();
        }

        /// <summary>
        /// Incluir Tributo
        /// </summary>
        [HttpPost, Route("{agencia}/{conta}/tributo")]
        public IActionResult Tributo(IncluirTributoCommand command)
        {
            return Ok();
        }

        /// <summary>
        /// Cancelar Tributo
        /// </summary>
        [HttpPut, Route("{agencia}/{conta}/tributo")]
        public IActionResult Tributo(AtualizarTributoCommand command)
        {
            return Ok();
        }
        #endregion
    }
}