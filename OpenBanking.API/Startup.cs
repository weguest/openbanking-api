﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.ReDoc;
using Swashbuckle.AspNetCore.Swagger;
using System.IO;

namespace OpenBanking.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "OpenBanking API V1", Version = "v1" });
                c.SwaggerDoc("v2", new Info { Title = "OpenBanking API V2", Version = "v2" });

                var filePath = Path.Combine(System.AppContext.BaseDirectory, "OpenBanking.API.xml");
                c.IncludeXmlComments(filePath);

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();

            app.UseSwagger();

            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            //    c.SwaggerEndpoint("/swagger/v2/swagger.json", "My API V2");
            //});

            //app.UseReDoc(c =>
            //{
            //    c.SpecUrl("/v1/swagger.json");
            //    c.UntrustedSpec();
            //    c.ScrollYOffset(10);
            //    c.HideHostname();
            //    c.HideDownloadButton());
            //    c.ExpandResponses("200,201");
            //    c.RequiredPropsFirst();
            //    c.NoAutoAuth();
            //    c.PathInMiddlePanel();
            //    c.HideLoading();
            //    c.NativeScrollbars();
            //    c.DisableSearch();
            //    c.OnlyRequiredInSamples();
            //    c.SortPropsAlphabetically();
            //});

            app.UseReDoc(c =>
            {
                c.SpecUrl = "/swagger/v2/swagger.json";

                c.RoutePrefix = "docs";

                c.ConfigObject = new ConfigObject()
                {
                    ExpandResponses = "200,204",
                    AdditionalItems = new Dictionary<string, object>()
                    {
                        {
                            "x-code-samples", new
                            {
                                lang = "JavaScript",
                                source = "console.log('Hello World');"
                            }
                        }
                    }
                };
                c.DocumentTitle = "My API Docs";
            });

        }
    }
}
